package Task1;
import java.util.*;

public class Game {
    List<Question> questions;

    Game(List<Question> questions) {
        this.questions = questions;
    }

    void start() {
        Scanner scanner = new Scanner(System.in);
        int score = 0;
        for (Question question : this.questions) {
            question.shuffleAnswers();
            System.out.println(question.question);
            for (int i = 0; i < question.answers.size(); i++) {
                System.out.println((i + 1) + ". " + question.answers.get(i));
            }
            int userAnswer = 0;
            while (true) {
                System.out.print("Введіть номер вашої відповіді: ");
                userAnswer = scanner.nextInt();
                if (userAnswer > 0 && userAnswer <= question.answers.size()) {
                    break;
                } else {
                    System.out.println("Ви ввели неправильний номер. Будь ласка, спробуйте ще раз.");
                }
            }
            if (question.answers.get(userAnswer - 1).equals(question.answers.get(question.correctAnswerIndex))) {
                System.out.println("Правильно!");
                score++;
            } else {
                System.out.println("Неправильно. Правильна відповідь була: " + question.answers.get(question.correctAnswerIndex));
            }
        }
        System.out.println("Вітаємо, ви набрали " + score + " з " + this.questions.size() + " балів!");
        scanner.close();
    }
}
