package Task1;
import java.util.*;

public class Question {
    String question;
    List<String> answers;
    int correctAnswerIndex;

    Question(String question, List<String> answers, int correctAnswerIndex) {
        this.question = question;
        this.answers = answers;
        this.correctAnswerIndex = correctAnswerIndex;
    }

    void shuffleAnswers() {
        String correctAnswer = answers.get(correctAnswerIndex);
        Collections.shuffle(this.answers);
        correctAnswerIndex = answers.indexOf(correctAnswer);
    }
}
