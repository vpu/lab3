package Task1;
import java.io.*;
import java.util.*;

public class Task1 {
    public static void main(String[] args) {
        List<Question> questions = new ArrayList<>();
        try {
            File file = new File("C:\\Users\\edena\\OneDrive\\Рабочий стол\\ВПУ111\\Java\\HW3\\src\\Task1\\questions.txt");
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] parts = line.split(";");
                List<String> answers = Arrays.asList(Arrays.copyOfRange(parts, 1, parts.length));
                int correctAnswerIndex = answers.indexOf(parts[1]);
                questions.add(new Question(parts[0], answers, correctAnswerIndex));
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("Файл з питаннями не знайдено.");
            e.printStackTrace();
        }
        Game game = new Game(questions);
        game.start();
    }
}