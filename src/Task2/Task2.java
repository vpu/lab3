package Task2;
import java.util.*;
import java.util.concurrent.*;

public class Task2 {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        int[] array = new int[100];
        ExecutorService executor = Executors.newFixedThreadPool(3);

        Future<?> future1 = executor.submit(() -> {
            Random random = new Random();
            for (int i = 0; i < array.length; i++) {
                array[i] = random.nextInt(100);
            }
        });

        future1.get();

        Future<Integer> future2 = executor.submit(() -> {
            int sum = 0;
            for (int i : array) {
                sum += i;
            }
            return sum;
        });

        Future<Integer> future3 = executor.submit(() -> {
            int min = Integer.MAX_VALUE;
            for (int i : array) {
                if (i < min) {
                    min = i;
                }
            }
            return min;
        });

        int sum = future2.get();
        int min = future3.get();

        System.out.println("Сума всіх чисел: " + sum);
        System.out.println("Найменше число: " + min);

        executor.shutdown();
    }
}
