package Task3;
import java.lang.reflect.*;
import java.util.Arrays;
import java.util.Date;

public class Task3 {
    public static void main(String[] args) throws Exception {
        // Створення копії рядка
        String originalString = "Hello, world!";
        String copyString = (String) copyObject(originalString);
        System.out.println(copyString);

        // Створення копії масиву цілих чисел
        int[] originalIntArray = {1, 2, 3, 4, 5};
        int[] copyIntArray = (int[]) copyObject(originalIntArray);
        System.out.println(Arrays.toString(copyIntArray));

        // Створення копії масиву рядків
        String[] originalStringArray = {"Hello", "world!"};
        String[] copyStringArray = (String[]) copyObject(originalStringArray);
        System.out.println(Arrays.toString(copyStringArray));

        // Спроба створити копію об'єкта типу Date
        Date originalDate = new Date();
        try {
            Date copyDate = (Date) copyObject(originalDate);
            System.out.println(copyDate);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static Object copyObject(Object obj) throws Exception {
        Class<?> claszs = obj.getClass();
        if (claszs.isArray()) {
            int length = Array.getLength(obj);
            Class<?> componentType = claszs.getComponentType();
            Object arrayCopy = Array.newInstance(componentType, length);
            System.arraycopy(obj, 0, arrayCopy, 0, length);
            return arrayCopy;
        } else if (obj instanceof String) {
            return new String((String) obj);
        } else {
            throw new Exception("Непідтримуваний тип об'єкта: " + claszs);
        }
    }
}
